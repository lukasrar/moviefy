
/* 1)*/
/* a)*/
DELIMITER |
create procedure sp_insert_playlist(playlistName varchar(255))
begin
insert into tb_playlist values
(null, playlistName);
END |
DELIMITER ;

/* b)*/
DELIMITER |
create procedure sp_insert_movie(movieName varchar(255), releaseDate date)
begin
insert into tb_movie values
(null, movieName, "",  releaseDate);
END |
DELIMITER ;


/* 2)*/
DELIMITER |		
create function func_raterPerUser(userId int)
returns int begin
declare @rates int;
set @rates = ( select count(user_id) from tb_rate where user_id = userId )
return @rates;
END |
DELIMITER ;


DELIMITER |
create function func_mostReviewedMovie()
returns varchar(255) begin
declare @movieName varchar(255)
set @movieName = ( select m.name, count(*) as times FROM tb_movie m
inner join tb_rate r
on  r.movie_id = m.id
group by m.name
order by times desc ;
)
return @movieName;
END |
DELIMITER ;

DELIMITER |
create function func_mostActiveUser()
returns int begin
declare @userId int
set @userId = ( select u.name, count(*) as times from tb_user u
inner join tb_rate r
on  u.id = r.user_id
group by r.user_id
order by times desc;
)
return @movieName;
END |
DELIMITER ;

