package com.moviefy.resources;

import com.moviefy.dtos.CredentialsDTO;
import com.moviefy.dtos.TokenDTO;
import com.moviefy.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/auth")
public class AuthResource {

    @Autowired
    private AuthService service;

    @PostMapping("/login")
    public ResponseEntity<TokenDTO> authenticate(@RequestBody CredentialsDTO credentialsDTO){
        TokenDTO tokenDTO = service.authenticate(credentialsDTO);
        return ResponseEntity.ok().body(tokenDTO);
    }
}
