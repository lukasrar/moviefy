package com.moviefy.resources;

import com.moviefy.dtos.MovieCommentsDTO;
import com.moviefy.dtos.MovieDTO;
import com.moviefy.dtos.MovieInsertDTO;
import com.moviefy.dtos.PlaylistDTO;
import com.moviefy.services.MovieService;
import com.moviefy.services.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping(value = "/playlist")
public class PlaylistResource {
    @Autowired
    private PlaylistService service;

    @PostMapping
    public ResponseEntity<PlaylistDTO> insert(@RequestBody PlaylistDTO dto) {
        PlaylistDTO playlistDTO = service.insert(dto);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(playlistDTO.getId()).toUri();

        return ResponseEntity.created(uri).body(playlistDTO);
    }
}
