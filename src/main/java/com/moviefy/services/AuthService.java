package com.moviefy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.moviefy.dtos.CredentialsDTO;
import com.moviefy.dtos.TokenDTO;
import com.moviefy.entities.User;
import com.moviefy.repositories.UserRepository;
import com.moviefy.security.JWTUtil;
import com.moviefy.services.exceptions.JWTAuthenticationException;
import com.moviefy.services.exceptions.JWTAuthorizationException;

@Service
public class AuthService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JWTUtil jwtUtil;
    
    @Autowired
    private UserRepository userRepository;

    @Transactional(readOnly = true)
    public TokenDTO authenticate(CredentialsDTO credentialsDTO) {
        try {
            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(credentialsDTO.getEmail(), credentialsDTO.getPassword());
            authenticationManager.authenticate(authToken);
            String token = jwtUtil.generateToken(credentialsDTO.getEmail());
            return new TokenDTO(credentialsDTO.getEmail(), token);
        } catch(AuthenticationException e) {
               throw new JWTAuthenticationException("Bad credentials");
        }
    }
    
    public User authenticated() {
        try {
            UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return userRepository.findByEmail(userDetails.getUsername());
        } catch (Exception exception) {
            throw new JWTAuthorizationException("Access denied.");
        }
    }
}
