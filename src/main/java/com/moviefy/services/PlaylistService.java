package com.moviefy.services;

import com.moviefy.dtos.MovieCommentsDTO;
import com.moviefy.dtos.MovieDTO;
import com.moviefy.dtos.MovieInsertDTO;
import com.moviefy.dtos.PlaylistDTO;
import com.moviefy.entities.Movie;
import com.moviefy.entities.Playlist;
import com.moviefy.repositories.MovieRepository;
import com.moviefy.repositories.PlaylistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PlaylistService {
    @Autowired
    private PlaylistRepository repository;


    public PlaylistDTO insert(PlaylistDTO dto) {
        Playlist playlist = new Playlist(null, dto.getTitle());
        for (Movie movie : dto.getMovies()) playlist.getMovies().add(new Movie(movie.getId(), null, null, null));
        repository.save(playlist);
        return new PlaylistDTO(playlist);
    }
}
