package com.moviefy.services;

import com.moviefy.dtos.MovieCommentsDTO;
import com.moviefy.dtos.MovieDTO;
import com.moviefy.dtos.MovieInsertDTO;
import com.moviefy.entities.Movie;
import com.moviefy.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
public class MovieService {
    @Autowired
    private MovieRepository repository;

    public Page<MovieDTO> findAllPaged(PageRequest pageRequest) {
        Page<Movie> movies = repository.findAll(pageRequest);
        return movies.map(MovieDTO::new);
    }

    @Transactional
    public MovieCommentsDTO findById(Long id) {
        return new MovieCommentsDTO(repository.getOne(id));
    }

    public MovieInsertDTO insert(MovieInsertDTO dto) {
        Movie newMovie = repository.save(dto.toEntity());
        return new MovieInsertDTO(newMovie);
    }
}
