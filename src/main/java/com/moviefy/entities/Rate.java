package com.moviefy.entities;

import com.moviefy.entities.pk.RatePK;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

@Entity
@Table(name = "tb_rate")
public class Rate implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private RatePK id = new RatePK();

    private Integer rate;
    private Instant createdAt;
    private Instant updatedAt;

    public Rate() {
    }

    public Rate(User user, Movie movie, Integer rate, Instant createdAt, Instant updatedAt) {
        this.id.setUser(user);
        this.id.setMovie(movie);
        this.rate = rate;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public RatePK getId() {
        return id;
    }

    public void setId(RatePK id) {
        this.id = id;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rate rate = (Rate) o;
        return Objects.equals(id, rate.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
