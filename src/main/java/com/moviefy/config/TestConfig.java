package com.moviefy.config;

import com.moviefy.entities.*;
import com.moviefy.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.Instant;
import java.util.Arrays;

@Configuration
@Profile("test")
public class TestConfig implements CommandLineRunner {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PlaylistRepository playlistRepository;

    @Autowired
    private RateRepository rateRepository;

    @Autowired
    private UserCommentLikeRepository userCommentLikeRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public void run(String... args) throws Exception {
        Role admin = new Role(null, "ROLE_ADMIN");
        Role client = new Role(null, "ROLE_CLIENT");
        this.roleRepository.saveAll(Arrays.asList(admin, client));

        User luiz = new User(null, "luiz", "luiz@email.com.br", passwordEncoder.encode("luiz123"));
        User lukas = new User(null, "lukas", "lukas@email.com.br", passwordEncoder.encode("lukas123"));
        User cleber = new User(null, "Cleber", "cleber@email.com.br", passwordEncoder.encode("cleber123"));

        luiz.getRoles().add(admin);
        luiz.getRoles().add(client);
        lukas.getRoles().add(admin);
        lukas.getRoles().add(client);
        cleber.getRoles().add(client);

        userRepository.save(luiz);
        userRepository.save(lukas);
        userRepository.save(cleber);


        Category sciFi = new Category(null, "Science Fiction");
        Category horror = new Category(null, "Horror");

        categoryRepository.save(sciFi);
        categoryRepository.save(horror);

        Movie starWars = new Movie(null, "Star Wars", Instant.parse("1972-01-01T00:00:00Z"), "");
        Movie aliens = new Movie(null, "Alien", Instant.parse("1977-01-01T00:00:00Z"), "");

        starWars.getCategories().add(sciFi);
        aliens.getCategories().add(sciFi);
        aliens.getCategories().add(horror);

        movieRepository.save(starWars);
        movieRepository.save(aliens);

        Comment goodComment = new Comment(null, "Such a good movie!", Instant.parse("1972-01-01T00:00:00Z"), starWars, luiz);
        Comment badComment = new Comment(null, "Such a bad movie!", Instant.parse("1972-01-01T00:00:00Z"), aliens, lukas);

        commentRepository.save(goodComment);
        commentRepository.save(badComment);

        UserCommentLike lukasLikedLuizComment = new UserCommentLike(lukas, goodComment, Instant.parse("1972-01-01T00:00:00Z"));

        userCommentLikeRepository.save(lukasLikedLuizComment);

        Playlist spacePlaylist = new Playlist(null, "Space Movies");

        spacePlaylist.getMovies().add(starWars);
        spacePlaylist.getMovies().add(aliens);

        playlistRepository.save(spacePlaylist);

        Rate luizStarWarsRate = new Rate(luiz, starWars, 5, Instant.parse("1972-01-01T00:00:00Z"), null);
        Rate lukasAliensRate = new Rate(lukas, aliens, 3, Instant.parse("1972-01-01T00:00:00Z"), null);

        rateRepository.save(luizStarWarsRate);
        rateRepository.save(lukasAliensRate);

    }
}
