package com.moviefy.repositories;

import com.moviefy.entities.UserCommentLike;
import com.moviefy.entities.pk.UserCommentLikePK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserCommentLikeRepository extends JpaRepository<UserCommentLike, UserCommentLikePK> {
}
