package com.moviefy.repositories;

import com.moviefy.entities.Rate;
import com.moviefy.entities.pk.RatePK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RateRepository extends JpaRepository<Rate, RatePK> {
}
