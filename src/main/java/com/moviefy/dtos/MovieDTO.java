package com.moviefy.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.moviefy.entities.Category;
import com.moviefy.entities.Movie;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class MovieDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT")
    private Instant releaseDate;
    private String photoUrl;


    private Set<Category> categories = new HashSet<>();

    public MovieDTO() {}

    public MovieDTO(Long id, String name, Instant releaseDate, String photoUrl) {
        this.id = id;
        this.name = name;
        this.releaseDate = releaseDate;
        this.photoUrl = photoUrl;
    }

    public MovieDTO(Movie movie) {
        this.id = movie.getId();
        this.name = movie.getName();
        this.releaseDate = movie.getReleaseDate();
        this.photoUrl = movie.getPhotoUrl();

        for(Category category : movie.getCategories()) this.categories.add(category);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Instant releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public Movie toEntity() {
        Movie movie = new Movie(id, name, releaseDate, photoUrl);
        for(Category category : categories) movie.getCategories().add(category);
        return movie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieDTO movieDTO = (MovieDTO) o;
        return Objects.equals(id, movieDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
